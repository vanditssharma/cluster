package com.coordinates.cluster;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Vector {

	public static void main(String[] args) {

		points();
	}

	public static void points() {
        
		System.out.println("Enter number of points you want:");
		Scanner in = new Scanner(System.in);
		int no = in.nextInt();  //Number of points
		
		Map<String, List<Integer>> map = new HashMap<>();
        
		System.out.println("Enter the key and the values:");
		
		for (int i = 0; i < no; i++) {
            
			//Storing list of key and there values
			List<Integer> com = new ArrayList<Integer>();
			String key = in.next();
			Integer x = in.nextInt();
			Integer y = in.nextInt();

			com.add(x);
			com.add(y);
			map.put(key, com);

		}

		 System.out.println("Key and Values you entered are:-");
		for (Map.Entry m : map.entrySet()) {
			System.out.println(m.getKey() + " " + m.getValue()); 
		}

		//Calling centroids class to make random point
		Centroids call = new Centroids();
		call.random(map);
	}

}
